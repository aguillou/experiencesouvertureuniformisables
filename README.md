# Experiences Ouverture Uniformisables


## Ensembles alpha-horizontaux dans le cylindre $|z|=1$, $|t|\leq \tan(alpha)$

Vous pouvez cliquer sur le bouton ci-dessous pour pouvoir expérimenter sur cet objet:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.inria.fr%2Faguillou%2Fexperiencesouvertureuniformisables/HEAD)

Attention: ça risque d'être très long, vu que ça installe SageMath sur un serveur...
