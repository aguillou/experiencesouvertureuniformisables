alpha, s, t, u, theta, eta, nu = var('alpha, s, theta, u, theta, eta,nu')

def b(theta,s,eta,t):
	if theta==eta:
		if s==t:
			raise ValueError('B non définie')
		else:
			return N(pi/2)
	else:
		return arctan(((s-t)+2*sin(theta-eta))/(-2 + 2 * cos(theta-eta)))
		
		
Trirapport(theta,s,eta,t,nu,u) = abs(b(theta,s,eta,t) + b(eta,t,nu,u) +b(nu,u,theta,s))
Tri_infty(theta,t,eta,s) = (abs(b(theta,t,eta,s)))
Tri_0(theta,t,eta,s) = abs(arctan(t)-arctan(s)+b(theta,t,eta,s))

def Zone1(alpha,p):
	(theta,s) = p
	a = tan(alpha)
	G1 = implicit_plot(alpha-Tri_infty(theta,s,eta,t), 
					   (eta,-pi,pi), (t,-a,a),plot_points = 1000,
					   fill = True,
					   frame=True, gridlines='minor',
					   alpha = .2)
	G2 = implicit_plot(alpha-Tri_0(theta,s,eta,t), 
					   (eta,-pi,pi), (t,-a,a),plot_points = 1000,
					   fill = True,
					   alpha=.2)
	G = G1+G2
	return G + point((theta,s), color = 'orange',size = 10,zorder=1)
	

def Zone2(alpha,p,q):
	a = tan(alpha)
	(theta,s) = p
	(eta,t) = q
	
	G1 = Zone1(alpha,(theta,s))
	G2 = Zone1(alpha,(eta,t))
	if Tri_infty(theta,s,eta,t)>alpha:
		print('Condition de trirapport non respectée avec infini pour p = %s, q=%s'%(p,q))
		G = G1+G2
	elif Tri_0(theta,s,eta,t)>alpha:
		print('Condition de trirapport non respectée avec 0 pour p = %s, q=%s'%(p,q))
		G = G1+G2
	else:
		G3 = implicit_plot(alpha-Trirapport(theta,s,eta,t,nu,u), 
					   (nu,-pi,pi), (u,-a,a),plot_points = 1000,
					   fill = True,
					   alpha = .2)
		G = G1+G2+G3
	G += points([(theta,s),(eta,t)], color = 'orange',size = 10,zorder=10)
	return G

def Zone(alpha,l):
	n=len(l)
	p0 = l[0]
	G = Zone1(alpha,p0)
	for p in l[1:]:
		G += Zone1(alpha,p)
	
	for i in range(n):
		p = l[i]
		for j in range(i+1,n):
			q= l[j]
			G += Zone2(alpha,p,q)
			for k in range(j+1,n):
				r=l[k]
				if Trirapport(p[0],p[1],q[0],q[1],r[0],r[1])>alpha:
					print('Condition de trirapport non respectée pour \
							p = %s, q=%s, r=%s'%(p,q,r))
	
	return G

	
print("Tout est chargé!")
